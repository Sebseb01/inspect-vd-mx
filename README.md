# Inspect VD MX

Ce dépot contient un peut de code afin d'analyser l'hebergeur des serveurs (MX) Mail, des communes vaudoises.

## La source des données

Dans le fichier wd-query.txt il y a la requête Wikidata qui retourne le nom des commune et le lien vers le site officiel.


Les données retournées sont dans les fichiers query.csv et query.json

## La moulinette

C'est dans inspect.php que la magie se passe ;-)

##ToDDo

Si ce projet devait avoir un avenir voici ce que je corrigerait

- Avoir une sortie de commande plus propre
- Amélioré les données Wikidata afin d'avoir plus de commune
- Ajouter la détéction en cas de CNAME

